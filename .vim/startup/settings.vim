execute pathogen#infect()

set nu
"set cc=81
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set background=dark
set cursorline
set cursorcolumn
set ruler
set wrap
set relativenumber
set list listchars=trail:§
set scrolloff=10
set hlsearch
set ffs=unix,dos
set mouse=a
set noshowmode


" Color Stuff
colorscheme monokai
syntax on

hi CursorLine term=none ctermfg=White ctermbg=none
hi CursorColumn term=none ctermfg=White ctermbg=none

"hi Search ctermbg=none ctermfg=none cterm=underline
"hi Normal ctermbg=none ctermfg=none cterm=underline
"hi CursorLine term=none cterm=none ctermbg=black
"hi CursorColumn term=none cterm=none ctermbg=black

"NERDtree
"autocmd vimenter * NERDTree

"lightline
set laststatus=2

"airline
"set laststatus=2
"let g:airline_powerline_fonts = 1
"let g:airline#extensions#tabline#enabled = 1
"let g:airline_theme='monokai'

"syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
